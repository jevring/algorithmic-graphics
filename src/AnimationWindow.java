import animations.*;

import javax.swing.*;
import java.awt.*;
import java.awt.image.MemoryImageSource;
import java.awt.image.ColorModel;
import java.awt.image.BufferedImage;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.util.Random;

public class AnimationWindow {
    private int width = 256;
    private int height = 256;
    final int[] imageData = new int[width * height];
    Random r = new Random();
    MemoryImageSource mis = new MemoryImageSource(width, height, ColorModel.getRGBdefault(), imageData, 0, width);
    BufferedImage img2 = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    Image img;
    Image laugh = Toolkit.getDefaultToolkit().createImage("E:\\!data\\download\\laughing-man.gif");
    FrameRenderer frameRenderer = new WormHoleFrameRenderer(width, height, imageData);
    volatile boolean running = false;

    JPanel canvas = new JPanel() {
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            // why do I have to paint in here? can't I just invoke it elsewhere? what if I don't want to paint right away?
            // it must be done here, because otherwise it is just overridden anyway.
            //g.setColor(Color.BLACK);
            //g.fillRect(0,0,width,height);
            //g.drawImage(img2, 0, 0, canvas);
            //System.out.println("kek");
            g.drawImage(img, 0, 0, canvas);
            //g.drawImage(laugh, 150, 150, canvas);
        }
    };

    public AnimationWindow() {
        mis.setAnimated(true);
        mis.setFullBufferUpdates(true);
        img = canvas.createImage(mis);

        JFrame w = new JFrame();
        w.setTitle("Animation thing");
        canvas.setPreferredSize(new Dimension(width,height));
        final JButton start = new JButton("Start");
        w.setLayout(new BorderLayout());
        w.add(canvas, BorderLayout.CENTER);
        w.add(start, BorderLayout.SOUTH);
        start.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new Thread() {
                    public void run() {
                        running = !running;
                        start.setText(running ? "Stop" : "Start");
                        if (running) {
                            animate();
                        }
                    }
                }.start();

            }
        });
        JComboBox renderers = new JComboBox();
        // the fastest one, by far, is the dual overlay plasma, since it pre-computes its color range. it's also the least flexible (in size), because of that.
        renderers.addItem(new HSBFullColorThrobberFrameRenderer(width, height, imageData));
        renderers.addItem(new WormHoleFrameRenderer(width, height, imageData));
        renderers.addItem(new DualOverlayPlasmaFrameRendererRemix(width, height, imageData));
        renderers.addItem(new DotNetThrobberPlasmaFrameRenderer(width, height, imageData));
        renderers.addItem(new PsychadelicHSBTHrobberFrameRenderer(width, height, imageData));
        renderers.addItem(new MovingHSBLinesFrameRenderer(width, height, imageData));
        renderers.addItem(new DualOverlayPlasmaFrameRenderer(width, height, imageData));
        renderers.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                frameRenderer = (FrameRenderer)e.getItem();
            }
        });
        w.add(renderers, BorderLayout.NORTH);
        w.setLocation(width,height);
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        w.pack();
        w.setVisible(true);
    }
    private void animate() {
        while(running) {
            frameRenderer.renderNextFrame();
            mis.newPixels();
            img.flush();
            //img2.flush();
            canvas.repaint();

            try {
                //System.out.println("sleeping " + i);
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        // todo: do a producer-consumer thing
        // "check out" work units via an atomic integer, render the, then put htem in a blocking queue. that way we can render a bunch of things in parallel
        // then put them in the queue. the renderer then waits n milliseconds before getting another rendered "picture" from the queue

    }

    public static void main(String[] args) {
        new AnimationWindow();
    }
}