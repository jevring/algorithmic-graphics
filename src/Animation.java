import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.awt.image.ColorModel;

public class Animation {
    int i;
    

    public static void main(String[] args) {
        new Animation();
    }

    public Animation() {
        final Dimension size = new Dimension(1600, 1200);
/*
        final int[] pixels = new int[size.width*size.height];
        final MemoryImageSource mis = new MemoryImageSource(size.width, size.height, ColorModel.getRGBdefault(), pixels, 0, size.width);
        mis.setAnimated(true);
        mis.setFullBufferUpdates(true);
*/
        final BufferedImage img = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_ARGB);
        JPanel p = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                int offset = size.height / 2;
                int amplitude = offset / 4;

                //Image image = createImage(mis);
                //g.setColor(Color.BLACK);
                //g.fillRect(0,0,size.width, size.height);
                /*
                final Graphics gg = img.getGraphics();

                */
                double s = Math.sin(Math.toRadians(i)) * 0.5;
                int rgbs = Color.HSBtoRGB((float) s, 1.0f, 1.0f);
/*

                img.setRGB(i, (int) (s * amplitude) + offset, rgbs);
                img.setRGB(i, (int) (s * amplitude) + offset + 1, rgbs);
                img.setRGB(i, (int) (s * amplitude) + offset + 2, rgbs);
                img.setRGB(i, (int) (s * amplitude) + offset + 3, rgbs);
                img.setRGB(i, (int) (s * amplitude) + offset + 4, rgbs);
                img.setRGB(i, (int) (s * amplitude) + offset + 5, rgbs);
                img.setRGB(i, (int) (s * amplitude) + offset + 6, rgbs);

                img.setRGB(i+1, (int) (s * amplitude) + offset, rgbs);
                img.setRGB(i+1, (int) (s * amplitude) + offset + 1, rgbs);
                img.setRGB(i+1, (int) (s * amplitude) + offset + 2, rgbs);
                img.setRGB(i+1, (int) (s * amplitude) + offset + 3, rgbs);
                img.setRGB(i+1, (int) (s * amplitude) + offset + 4, rgbs);
                img.setRGB(i+1, (int) (s * amplitude) + offset + 5, rgbs);
                img.setRGB(i+1, (int) (s * amplitude) + offset + 6, rgbs);

                img.setRGB(i+2, (int) (s * amplitude) + offset, rgbs);
                img.setRGB(i+2, (int) (s * amplitude) + offset + 1, rgbs);
                img.setRGB(i+2, (int) (s * amplitude) + offset + 2, rgbs);
                img.setRGB(i+2, (int) (s * amplitude) + offset + 3, rgbs);
                img.setRGB(i+2, (int) (s * amplitude) + offset + 4, rgbs);
                img.setRGB(i+2, (int) (s * amplitude) + offset + 5, rgbs);
                img.setRGB(i+2, (int) (s * amplitude) + offset + 6, rgbs);

                img.setRGB(i+3, (int) (s * amplitude) + offset, rgbs);
                img.setRGB(i+3, (int) (s * amplitude) + offset + 1, rgbs);
                img.setRGB(i+3, (int) (s * amplitude) + offset + 2, rgbs);
                img.setRGB(i+3, (int) (s * amplitude) + offset + 3, rgbs);
                img.setRGB(i+3, (int) (s * amplitude) + offset + 4, rgbs);
                img.setRGB(i+3, (int) (s * amplitude) + offset + 5, rgbs);
                img.setRGB(i+3, (int) (s * amplitude) + offset + 6, rgbs);

                img.setRGB(i+4, (int) (s * amplitude) + offset, rgbs);
                img.setRGB(i+4, (int) (s * amplitude) + offset + 1, rgbs);
                img.setRGB(i+4, (int) (s * amplitude) + offset + 2, rgbs);
                img.setRGB(i+4, (int) (s * amplitude) + offset + 3, rgbs);
                img.setRGB(i+4, (int) (s * amplitude) + offset + 4, rgbs);
                img.setRGB(i+4, (int) (s * amplitude) + offset + 5, rgbs);
                img.setRGB(i+4, (int) (s * amplitude) + offset + 6, rgbs);
*/
                
                //Color.getHSBColor()

                g.setColor(new Color(rgbs));
                for (int j = 0; j < 30; j++) {
                    g.drawOval((size.width / 2)-i-j, (size.height / 2)-i-j, (i*2)+(j*2), (i*2)+(j*2));
                }


                // todo: we want a nice pulsing image that just keeps on going, where the colors move out, like ripples on water when something is dropped
                // http://freespace.virgin.net/hugo.elias/graphics/x_water.htm

                // todo: when we know how to animate this, make a cool sinus scroller!

//                g.drawImage(img, 0, 0, null);
            }

            @Override
            public Dimension getMinimumSize() {
                return size;
            }

            @Override
            public Dimension getPreferredSize() {
                return size;
            }
        };
        JFrame w = new JFrame();
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        w.add(p);
        w.pack();
        w.setVisible(true);
        final Graphics gg = img.getGraphics();
        gg.setColor(Color.BLACK);
        gg.fillRect(0,0,size.width, size.height);
        for (i = 0; i < size.width - 6; i++) {
            // todo:make it keep moving, so that it looks like a spring
            // for that we need to basically move the wrole picture, or something
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            w.repaint();
        }
        System.out.println("done");
    }
}
