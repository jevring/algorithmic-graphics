package animations;

import java.awt.*;

public class DualOverlayPlasmaFrameRendererRemix implements FrameRenderer {
    private final int width;
    private final int height;
    private final int[] imageData;
    private Color[] colors = new Color[256];
    private int[] plasma1 = new int[512 * 512];
    private int[] plasma2 = new int[512 * 512];

    private int px = 256;
    private int p2x = 45;
    private int ll = 0;

    public DualOverlayPlasmaFrameRendererRemix(int width, int height, int[] imageData) {
        this.width = width;
        this.height = height;
        this.imageData = imageData;
        // for old java plasma:
        int cr = 0;
        int cg = 0;
        int cb = 255;

        // create a palette for us.
        // later, the "height map" (the plasmas overlayed) will determine which color we'll choose from this map
        // it looks too smooth to only be 256 colors, but maybe that's because it's only yellow ad blue that shift
        for (int i = 0; i < 256; i++) {
            // R + G + B

            if (i > 128) {
                colors[i] = Color.BLACK;
            } else {
                colors[i] = Color.WHITE;
            }

            // this code simply makes a gradient.
            // lets graph it along one asis and see what it looks like
            // it's a nice blue to yelow gradient, as could be expected, due to the way the plasma looks.
/*
            colors[i] = new Color(cr, cg, cb);

            if (cb < 128) {
                cb--;
            } else {
                cb -= 2;
            }
            if (cb < 0) cb = 0;
            if (cb > 200) {
                cg++;
                cr++;
            }
            if (cr > 64) cg++;
            if (cb < 128) cr++;
*/
            //System.out.println(cr + ";" + cg + ";" + cb);

        }


        int dst = 0;
        for (int i = 0; i < 512; i++) {
            for (int j = 0; j < 512; j++) {
//                plasma1[dst] = (int)( 64 + 63 *(Math.sin((double)Math.sqrt((double)((256-j)*(256-j)+(256-i)*(256-i))) /16 )));
//                plasma2[dst] = (int)( 64 + 63 *(Math.sin((double)Math.sqrt((double)((128-j)*(128-j)+(200-i)*(200-i))) /57 )));
//                plasma1[dst] = (int) (64 + 63 * Math.sin((double) i / (38 + 14 * Math.cos((double) j / 70))) * Math.cos((double) j / (33 + 15 * Math.sin((double) i / 60))));
//                plasma2[dst] = (int) (64 + 63 * Math.sin((double) i / (37 + 15 * Math.cos((double) j / 74))) * Math.cos((double) j / (31 + 11 * Math.sin((double) i / 57))));
//                plasma1[dst] = (int) (64 + 63 * Math.sin((double) i / (38 + 14 * Math.cos((double) j / 70))));
//                plasma2[dst] = (int) (64 + 63 * Math.sin((double) i / (37 + 15 * Math.cos((double) j / 74))));
                plasma1[dst] = (int) ((Math.sin((double) (i * 512)+j)) % 255);
                plasma2[dst] = (int) ((Math.sin((double) (i * 512)+j)) % 255);
                dst++;
            }
        }
    }

    public void renderNextFrame() {
        // we get the effect of this function by moving two plasma patterns on top over each other (wax on wax off style) and then combining the values.
        int color;
        // what these variables do is to make sure the thing rotates. that's how we get the moving effect.
        int lx = 128 + (int) (120 * Math.sin((px * 0.01745)));
        int l2x = 128 + (int) (120 * Math.sin((p2x * 0.01745)));
        int ly = 128 + (int) (120 * Math.cos((px * 0.01745)));
        int l2y = 128 + (int) (120 * Math.cos((p2x * 0.01745)));

        for (int y = 0; y < height; y++) { // rows, y
            for (int x = 0; x < width; x++) { // cols, x
                //color = plasma1[(lx + x) + (y + ly) * 512] + plasma2[(x + l2x) + (y + l2y) * 512];
                color = plasma2[(lx + x) + (y + ly) * 512] * 2;
                imageData[(y * width) + x] = colors[color].getRGB();

            }
        }
        p2x--;
        if (p2x < -360) p2x = 0;
        if (ll == 0) px++;
        if (px > 360) {
            px = 0;
        } else {
            ll++;
        }
        if (ll > 1) ll = 0;
    }


    public String toString() {
        return getClass().getName();
    }
}