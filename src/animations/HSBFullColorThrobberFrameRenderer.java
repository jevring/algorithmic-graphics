package animations;

import java.awt.*;

public class HSBFullColorThrobberFrameRenderer implements FrameRenderer {
    private final int width;
    private final int height;
    private final int[] imageData;
    private int offset = 10000;
    private int increment = 100;

    public HSBFullColorThrobberFrameRenderer(int width, int height, int[] imageData) {
        this.width = width;
        this.height = height;
        this.imageData = imageData;
    }

    public void renderNextFrame() {
        offset += increment;
        float hue = (float) (offset) / (width * height);
        int color = Color.HSBtoRGB(hue, 1.0f, 1.0f);
        for (int i = 0; i < width*height; i++) {
            imageData[i] = color;
        }
    }

    public String toString() {
        return getClass().getName();
    }
}