package animations;

import java.awt.*;

public class PsychadelicHSBTHrobberFrameRenderer implements FrameRenderer {
    private final int width;
    private final int height;
    private final int[] imageData;
    private int offset = 10000;
    private int increment = 5;

    public PsychadelicHSBTHrobberFrameRenderer(int width, int height, int[] imageData) {
        this.width = width;
        this.height = height;
        this.imageData = imageData;
    }

    public void renderNextFrame() {
        offset += increment;
        for (int i = 0; i < height; i++) { // rows
            for (int j = 0; j < width; j++) { // cols
                float hue = (float) ImprovedNoise.noise(i, j, i*j + offset);
                hue = (float)Math.sin(Math.toRadians(hue));
                //System.out.println(hue);
                imageData[(i * width) + j] = Color.HSBtoRGB(hue, 1.0f, 1.0f);
                //imageData[(i * width) + j] = animations.ImprovedNoise.noise(i, j, 255);
                //imageData[(i * width) + j] = animations.ImprovedNoise.noise(i % 255, j % 255, offset % 255);
                //System.out.println(animations.ImprovedNoise.noise(i, j, 255) << 4);
                //img2.setRGB(j, i, 255);

            }
        }
    }
    
    public String toString() {
        return getClass().getName();
    }
}
