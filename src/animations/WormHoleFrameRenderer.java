package animations;

public class WormHoleFrameRenderer implements FrameRenderer {
    private final int width;
    private final int height;
    private final int[] imageData;
    private int radius = 10;
    private final int cx; // center x
    private final int cy; // center y
    private final int rings;
    private final int[] radiuses;
    private final int ringSpeed = 3;

    public WormHoleFrameRenderer(int width, int height, int[] imageData) {
        this.width = width;
        this.height = height;
        this.imageData = imageData;
        cx = width / 2;
        cy = height / 2;
        rings = cx / (ringSpeed* 10);
        radiuses = new int[rings];
        seedRings();
        // todo: now that seems to work. now EVERYTHING needs to be dependent on where we put the "center", but things hsould (probably?) still be anchored where they are
    }

    private void seedRings() {
        for (int i = 0; i < radiuses.length; i++) {
            radiuses[i] = (cx / 2) * (i + 1);
        }
    }


    public void renderNextFrame() {
        moveRings();
        radius+=3;
        //radius = radius % cx; // we can't do this, since we want the circle to fill out the corners as well.
        radius = radius % width;

        for (int y = 0; y < height; y++) { // rows
            for (int x = 0; x < width; x++) { // cols
                imageData[(y * width) + x] = 0xFF000000; // black
                // print circle
                if (pointIsOnARing(y, x)) {
                    imageData[(y * width) + x] = 0xFFFFFFFF; // white
                }
                // print guide lines
                if (x == cx || y == cy) {
                    imageData[(y * width) + x] = 0xFFFFFFFF; // white
                }
                // print diagonal guide lines
                //if (x == y) { // this only works when we are aiming for the middle. it needs to invokve x somehow
                if (x == y || x == width - y) {
                    // todo: this needs to be 4 lines that start from each corner and reach in to the center instead (preferrably bent)
                    imageData[(y * width) + x] = 0xFFFFFFFF; // white
                }
                // now the problem becomes making an oval
            }
        }
    }

    private void moveRings() {
        for (int i = 0; i < radiuses.length; i++) {
            //int radius = radiuses[i];
            radiuses[i] += ringSpeed;
            radiuses[i] %= width;
        }
    }

    private boolean pointIsOnARing(int y, int x) {
        //return distanceFromCenter(x, y) == radius;
        for (int r : radiuses) {
            if (r == distanceFromCenter(x, y)) {
                return true;
            }
        }
        return false;
    }

    private int distanceFromCenter(int x, int y) {
        // a point is on the circle if it is [radius] length units from the center
        // a^2 + b^2 = c^2
        // c == radius
        // c = sqrt(a^2 + b^2)
        // holy shit! I got it right the first time!
        return (int) Math.sqrt(Math.pow(x - cx, 2) + Math.pow(y - cy, 2));
    }

    public String toString() {
        return getClass().getName();
    }
}
