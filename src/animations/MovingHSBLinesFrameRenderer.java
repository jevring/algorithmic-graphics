package animations;

import java.awt.*;

public class MovingHSBLinesFrameRenderer implements FrameRenderer {
    private final int width;
    private final int height;
    private final int[] imageData;
    private int offset = 10000;
    private int increment = 5;

    public MovingHSBLinesFrameRenderer(int width, int height, int[] imageData) {
        this.width = width;
        this.height = height;
        this.imageData = imageData;
    }

    public void renderNextFrame() {
        offset += increment;
        for (int i = 0; i < width*height; i++) {
            float hue = (float) (i+offset) / (width * height);
            imageData[i] = Color.HSBtoRGB(hue, 1.0f, 1.0f);
        }
    }


    public String toString() {
        return getClass().getName();
    }
}
