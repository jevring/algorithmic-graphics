package animations;

import java.awt.*;

public interface FrameRenderer {
    public void renderNextFrame();
}
