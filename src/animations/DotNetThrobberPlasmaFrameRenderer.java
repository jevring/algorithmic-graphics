package animations;

public class DotNetThrobberPlasmaFrameRenderer implements FrameRenderer {
    private final int width;
    private final int height;
    private final int[] imageData;

    public DotNetThrobberPlasmaFrameRenderer(int width, int height, int[] imageData) {
        this.width = width;
        this.height = height;
        this.imageData = imageData;
    }

    private int inc = 0;
    public void renderNextFrame() {
        double x, y;
        inc++;
        for (int i = 0; i < height; i++) { // rows
            x = 20 * Math.sin(Math.toRadians((i * 4) + inc)) + 30 * Math.sin(Math.toRadians(i + inc * 4)) + 50 * Math.sin(Math.toRadians((i / 4) + (inc / 4)));
            for (int j = 0; j < width; j++) { // cols
                y = 40 * Math.sin(Math.toRadians((j * 6) + inc)) + 40 * Math.sin(Math.toRadians(j + inc * 6)) + 20 * Math.sin(Math.toRadians((j / 6) + (inc / 6)));
                int color = (int) (x+y) - 15000;
                // todo: how do I fill in the background with something else other than gray?
                //color = Color.HSBtoRGB((float) (x + y), 1.0f, 1.0f);
                imageData[(i * width) + j] = color;
            }
        }
    }


    public String toString() {
        return getClass().getName();
    }
}
