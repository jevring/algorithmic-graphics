import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class View {
    public View() {
        JPanel p = new JPanel() {
            private final Dimension size = new Dimension(1600, 1200);
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                /*
                for (int i = 0; i < size.width; i++) {
                    for (int j = 0; j < size.height; j++) {
                        g.setColor(new Color(i*j));
                        g.fillRect(i,j,1,1);
                    }
                }
                */
                /*
                for (int i = 0; i < size.width; i++) {
                    for (int j = 0; j < size.height; j++) {
                        int v = 0;
                        v = (int) (Math.sin(Math.toRadians(i)) * 255);
                        g.setColor(new Color(0, 0, Math.abs(v % 255)));
                        g.fillRect(i,j,1,1);
                    }
                }
                */
                /*
                g.setColor(Color.BLACK);
                int amplitude = 50;
                int offset = 200;
                for (int i = 0; i < size.width; i++) {
                    double v = Math.sin(Math.toRadians(i));
                    g.fillRect(i, (int) (v * amplitude) + offset,1,1);
                    g.fillRect(i, offset,1,1);
                }
                */
                /* this takes 2641 ms
                long start = System.currentTimeMillis();
                int offset = size.height / 2;
                int amplitude = offset / 2;
                BufferedImage img = new BufferedImage(size.width, size.height, BufferedImage.TYPE_3BYTE_BGR);
                img.getGraphics().setColor(Color.WHITE);
                img.getGraphics().fillRect(0,0,size.width, size.height);
                for (int i = 0; i < size.width; i++) {
                    double v = Math.sin(Math.toRadians(i));
                    img.setRGB(i, (int) (v * amplitude) + offset, 0);
                    for (int j = 1; j < 5000; j++) {
                        img.setRGB(i, (int) (Math.pow(v,j) * amplitude) + offset, i*j);
                    }


                }
                g.drawImage(img, 0,0,null);
                long stop = System.currentTimeMillis();
                System.out.println("took " + (stop - start) + " milliseconds");
                */
                /* this takes 4700 ms
                long start = System.currentTimeMillis();
                int offset = size.height / 2;
                int amplitude = offset / 2;
                g.setColor(Color.WHITE);
                g.fillRect(0,0,size.width, size.height);
                for (int i = 0; i < size.width; i++) {
                    double v = Math.sin(Math.toRadians(i));
                    g.fillRect(i, (int) (v * amplitude) + offset, 1,1);
                    for (int j = 1; j < 5000; j++) {
                        g.setColor(new Color(i*j));
                        g.fillRect(i, (int) (Math.pow(v,j) * amplitude) + offset, 1,1);
                    }
                }
                long stop = System.currentTimeMillis();
                System.out.println("took " + (stop - start) + " milliseconds");
                */
                /*  this took 3969 ms
                long start = System.currentTimeMillis();
                int offset = size.height / 2;
                int amplitude = offset / 2;
                g.setColor(Color.WHITE);
                g.fillRect(0,0,size.width, size.height);
                for (int i = 0; i < size.width; i++) {
                    double v = Math.sin(Math.toRadians(i));
                    int y = (int) (v * amplitude) + offset;
                    g.drawLine(i, y, i+1,y+1);
                    for (int j = 1; j < 5000; j++) {
                        g.setColor(new Color(i*j));
                        int y1 = (int) (Math.pow(v, j) * amplitude) + offset;
                        g.drawLine(i, y1, i+1,y1+1);
                    }
                }
                long stop = System.currentTimeMillis();
                System.out.println("took " + (stop - start) + " milliseconds");
                  */
                long start = System.currentTimeMillis();
                int offset = size.height / 2;
                int amplitude = offset / 4;
                BufferedImage img = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_ARGB);
                final Graphics gg = img.getGraphics();
                gg.setColor(Color.BLACK);
                gg.fillRect(0,0,size.width, size.height);
                int brushWidth = 3; // the higher this is, the morwarped the image becomes
                for (int i = 0; i < size.width; i++) {
                    final double bandWidth = 0.5; // how thick the circles should be. higher value = thinner circles. up to 1.0
                    double s = Math.sin(Math.toRadians(i)) * bandWidth;
                    double c = Math.cos(Math.toRadians(i)) * bandWidth;
                    // hue: x
                    // saturation: 100
                    // brightness: 100

                    int rgbs = Color.HSBtoRGB((float) s, 1.0f, 1.0f);
                    int rgbc = Color.HSBtoRGB((float) c, 1.0f, 1.0f);
                    img.setRGB(i, (int) (s * amplitude) + offset, rgbs);
                    img.setRGB(i, (int) (s * amplitude) + offset + 1, rgbs);
                    img.setRGB(i, (int) (s * amplitude) + offset + 2, rgbs);
                    img.setRGB(i, (int) (s * amplitude) + offset + 3, rgbs);
                    img.setRGB(i, (int) (s * amplitude) + offset + 4, rgbs);
                    img.setRGB(i, (int) (s * amplitude) + offset + 5, rgbs);
                    img.setRGB(i, (int) (s * amplitude) + offset + 6, rgbs);
                    img.setRGB(i, offset, rgbc);
                    img.setRGB(i, offset+1, rgbc);
                    img.setRGB(i, offset+2, rgbc);
                    img.setRGB(i, offset+3, rgbc);
                    img.setRGB(i, offset+4, rgbc);
                    img.setRGB(i, offset+5, rgbc);
                    // todo: when we know how to animate this, make a cool sinus scroller!

                    /*
                    gg.setColor(new Color(rgbs));
                    for (int j = 0; j <= brushWidth; j++) {
                        gg.drawOval((size.width / 2)-i+j, offset-i+j, (i*2)+j, (i*2)+j);
                    }
                    */
                }
                g.drawImage(img, 0, 0, null);

                long stop = System.currentTimeMillis();
                System.out.println("took " + (stop - start) + " milliseconds");
            }

            @Override
            public Dimension getMinimumSize() {
                return size;
            }

            @Override
            public Dimension getPreferredSize() {
                return size;
            }
        };
        JFrame w = new JFrame();
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        w.add(p);
        w.pack();
        w.setVisible(true);
        //w.setSize(400, 400);
    }


    public static void main(String[] args) {
        new View();
    }

}

