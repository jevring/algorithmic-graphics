import animations.ImprovedNoise;

import javax.swing.*;
import java.awt.*;
import java.awt.image.MemoryImageSource;
import java.awt.image.ColorModel;
import java.awt.image.BufferedImage;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Random;

public class Animation2 {
    private int width = 200;
    private int height = 200;
    final int[] imageData = new int[width * height];
    Random r = new Random();
    MemoryImageSource mis = new MemoryImageSource(width, height, ColorModel.getRGBdefault(), imageData, 0, width);
    BufferedImage img2 = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    Image img;
    Image laugh = Toolkit.getDefaultToolkit().createImage("E:\\!data\\download\\laughing-man.gif");

    JPanel canvas = new JPanel() {
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            // why do I have to paint in here? can't I just invoke it elsewhere? what if I don't want to paint right away?
            // it must be done here, because otherwise it is just overridden anyway.
            //g.setColor(Color.BLACK);
            //g.fillRect(0,0,width,height);
            //g.drawImage(img2, 0, 0, canvas);
            //System.out.println("kek");
            g.drawImage(img, 0, 0, canvas);
            //g.drawImage(laugh, 150, 150, canvas);
        }
    };

    public Animation2() {
        mis.setAnimated(true);
        mis.setFullBufferUpdates(true);
        img = canvas.createImage(mis);

        JFrame w = new JFrame();
        canvas.setPreferredSize(new Dimension(width,height));
        JButton start = new JButton("Start");
        w.setLayout(new BorderLayout());
        w.add(canvas, BorderLayout.CENTER);
        w.add(start, BorderLayout.SOUTH);
        start.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new Thread() {
                    public void run() {
                        Animation2.this.start();
                    }
                }.start();

            }
        });
        w.setLocation(height,height);
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        w.pack();
        w.setVisible(true);
    }

    private int offset = 10000;
    private int increment = 5;
    private void start() {
        // for old java plasma:
        int cr = 0;
        int cg = 0;
        int cb = 255;

        for (int i = 0; i < 256; i++)
        {
            // R + G + B
            colors[i] = new Color(cr, cg, cb);

            if (cb < 128)
            {
                cb--;
            }
            else
            {
                cb -= 2;
            }
            if (cb < 0) cb = 0;
            if (cb > 200)
            {
                cg++;
                cr++;
            }
            if (cr > 64) cg++;
            if (cb < 128) cr++;
            //Console.Out.WriteLine(cr + ";" + cg + ";" + cb);
        }


        int dst = 0;
        for (int i = 0; i < 512; i++)
        {
            for (int j = 0; j < 512; j++)
            {
//					           plasma1[dst] = (int)( 64 + 63 *(Math.sin((double)Math.sqrt((double)((256-j)*(256-j)+(256-i)*(256-i))) /16 )));
//					           plasma2[dst] = (int)( 64 + 63 *(Math.sin((double)Math.sqrt((double)((128-j)*(128-j)+(200-i)*(200-i))) /57 )));
                plasma1[dst] = (int)( 64 + 63 * Math.sin((double)i/(38+14*Math.cos((double)j/70)) )* Math.cos((double)j/(33+15*Math.sin((double)i/60))) );
                plasma2[dst] = (int)( 64 + 63 * Math.sin((double)i/(37+15*Math.cos((double)j/74)) )* Math.cos((double)j/(31+11*Math.sin((double)i/57))) );
                dst++;
            }
        }
        //drawFrame();

        boolean up = true;
        while(true) {
            if (offset >= 65535 * 2) {
                up = false;
            } else if (offset <= 0) {
                up = true;
            }

            if (up) {
                offset += increment;
            } else {
                offset -= increment;
            }

            //System.out.println(offset);
            drawFrame();
            try {
                //System.out.println("sleeping " + i);
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        // todo: do a producer-consumer thing
        // "check out" work units via an atomic integer, render the, then put htem in a blocking queue. that way we can render a bunch of things in parallel
        // then put them in the queue. the renderer then waits n milliseconds before getting another rendered "picture" from the queue

    }

    Color[] colors = new Color[256];
    int [] plasma1 = new int[512*512];
    int [] plasma2 = new int[512*512];

    int px=256;
    int py=256;
    int p2x=45;
    int p2y=256;
    int l2x;
    int l2y;
    int lx;
    int ly;
    int ll=0;
    private void drawFrame3() {
        // we get the effect of this function by moving two plasma patterns on top over each other (wax on wax off style) and then combining the values.
        int color = 0;
        lx=128+(int)(120*Math.sin((px*0.01745)));
        l2x=128+(int)(120*Math.sin((p2x*0.01745)));
        ly=128+(int)(120*Math.cos((px*0.01745)));
        l2y=128+(int)(120*Math.cos((p2x*0.01745)));

        for (int y = 0; y < height; y++) { // rows, y
            for (int x = 0; x < width; x++) { // cols, x
                color = plasma1[(lx+x)+(y+ly)*512]+plasma2[(x+l2x)+(y+l2y)*512];
                imageData[(y * width) + x] = colors[color].getRGB();

            }
        }
        p2x--;
        if (p2x<-360) p2x=0;
        if(ll==0) px++;
        if (px>360) {px=0;}
        else {ll++;}
        if(ll>1) ll=0;

        mis.newPixels();
        img.flush();
        //img2.flush();
        canvas.repaint();
    }

    private void drawFrame2() {
        for (int i = 0; i < width*height; i++) {
            float hue = (float) (i+offset) / (width * height);
            imageData[i] = Color.HSBtoRGB(hue, 1.0f, 1.0f);
        }
        mis.newPixels();
        img.flush();
        //img2.flush();
        canvas.repaint();
    }

    private void drawFrame4() {
        for (int i = 0; i < height; i++) { // rows
            for (int j = 0; j < width; j++) { // cols
                float hue = (float) ImprovedNoise.noise(i, j, i*j + offset);
                hue = (float)Math.sin(Math.toRadians(hue));
                //System.out.println(hue);
                imageData[(i * width) + j] = Color.HSBtoRGB(hue, 1.0f, 1.0f);
                //imageData[(i * width) + j] = animations.ImprovedNoise.noise(i, j, 255);
                //imageData[(i * width) + j] = animations.ImprovedNoise.noise(i % 255, j % 255, offset % 255);
                //System.out.println(animations.ImprovedNoise.noise(i, j, 255) << 4);
                //img2.setRGB(j, i, 255);

            }
        }
        /*
        for (int i = 0; i < width*height; i++) {
            float hue = (float) (i+offset) / (width * height);
            imageData[i] = Color.HSBtoRGB(hue, 1.0f, 1.0f);
        }
        */
        mis.newPixels();
        img.flush();
        //img2.flush();
        canvas.repaint();
    }

    int inc = 0;
    private void drawFrame() {
        double x, y;
        inc++;
        for (int i = 0; i < height; i++) { // rows
            x = 20 * Math.sin(Math.toRadians((i * 4) + inc)) + 30 * Math.sin(Math.toRadians(i + inc * 4)) + 50 * Math.sin(Math.toRadians((i / 4) + (inc / 4)));
            for (int j = 0; j < width; j++) { // cols
                y = 40 * Math.sin(Math.toRadians((j * 6) + inc)) + 40 * Math.sin(Math.toRadians(j + inc * 6)) + 20 * Math.sin(Math.toRadians((j / 6) + (inc / 6)));
                int color = (int) (x+y) - 15000;
                // todo: how do I fill in the background with something else other than gray?
                //color = Color.HSBtoRGB((float) (x + y), 1.0f, 1.0f);
                imageData[(i * width) + j] = color;
            }
        }
        mis.newPixels();
        img.flush();
        canvas.repaint();
    }

    public static void main(String[] args) {
        new Animation2();
    }
}
